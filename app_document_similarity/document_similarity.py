# NTLK functions
import nltk
from nltk.corpus import stopwords
from nltk.stem.snowball import SnowballStemmer # load nltk's SnowballStemmer as variabled 'stemmer'
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.metrics.pairwise import cosine_similarity
import re
import pandas as pd
from flask import Flask, url_for, request
import numpy as np
import json
# List of stopwords
stopwords= stopwords.words('english')
app = Flask(__name__)

@app.route('/document_sim_test')
def document_sim_test():
    #count = redis.incr('hits')
    return 'Document Similarity API working'

@app.route('/document_similarity', methods=['POST'])
def document_similarity():

    if request.method == 'POST':
        #all_data = pd.read_csv('/Users/prajwalshreyas/Desktop/Singularity/dockerApps/Blogs/TwitterAnalysis/Output/tweets_sent_ner_keyword.csv')[0:1000]
        all_data = pd.DataFrame(request.json)
        text_data = all_data[['text','ref']]
        text_data = text_data.dropna()

        # get the parametrs i.e. similarity score for clustering
        params = all_data[['similarity_threshold']]
        params = params.dropna()
        similarity_threshold = params['similarity_threshold'][0]

       # get count of words in the input
        text = text_data['text']
        count_words = text.str.split().str.len().sum()

        if count_words<13000:

            clustered_text = get_clusters(text_data,similarity_threshold)
            #clustered_text = clustered_text[['ref','cluster_ref']]
            # Cluster output format
            #cluster_out = pd.merge(text_data, clustered_text, on = 'ref')
            cluster_out = clustered_text[['ref','cluster_ref']]
            #Convert df t dict and the to Json
            cluster_out = cluster_out.to_dict(orient='records')
            cluster_out_json = json.dumps(cluster_out, ensure_ascii=False)

            print ('OUT JSON')

        else:

            cluster_out_json = "The character limit for a single API call is 13,000. Please reduce the data and retry."

        return cluster_out_json



# Function to cluster based on text
''' Within the for loop going through each distance array
a) Select all documents with distance less than the set threshold
b) Get the position of similar documents using 'np.where'
c) Take only the first element list of the tuple using '0' value
'''
def cluster_text(dist_text,processedTweets,dist_threshold_txt):
    cluster_id = 1
    for i in range(dist_text.shape[0]):
        try:
            doc_dist_array_txt = dist_text[i,]
            # identify document ids where the tweets are similar
            similarDocs = np.where(doc_dist_array_txt <= dist_threshold_txt)[0]
             #str(uuid.uuid1())
            # Filter for document number in index using 'similarDocs' variable, consider only the column 'Cluster_level1' where the value in 'None'
            # and assgn the 'cluster_id' to those rows which are meeting the above criterion
            processedTweets.ix[processedTweets.index.isin(similarDocs) & processedTweets['cluster_ref'].isin([None]), 'cluster_ref'] = cluster_id
            cluster_id = cluster_id + 1
        except ValueError:
            continue

    return processedTweets



# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Calculate Cosine distance of documents %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
           #------------------------------------ Count Vectorizer
#Function for Tokenizer
def tokenize_only(text):
    # first tokenize by sentence, then by word to ensure that punctuation is caught as it's own token
    tokens = [word.lower() for sent in nltk.sent_tokenize(text) for word in nltk.word_tokenize(sent)]
    filtered_tokens = []
    # filter out any tokens not containing letters (e.g., numeric tokens, raw punctuation)
    for token in tokens:
        if re.search('[a-zA-Z]', token):
            filtered_tokens.append(token)
    return filtered_tokens

def get_clusters(processedTweets, similarity_threshold):
       tweets = processedTweets['text'].tolist()

       #Define count vectorizer parameters
       vectorizer =  CountVectorizer(max_df=1.0, min_df=1, stop_words=stopwords, tokenizer=tokenize_only) # function to get the term frequency matrix
       # Get document term frequency matix
       dtf_matrix = vectorizer.fit_transform(tweets) #fit the vectorizer to tweets which does not contain'htttp' and 'RT'
       dist_text = np.around(abs(1 - cosine_similarity(dtf_matrix)),2)


       # ------------------------------------ 1D clustering based on distance measure
       # Pre clustering setup
       processedTweets['cluster_ref'] = None # Tweets that are 1-similarithy% similar, as defined by dist_threshold
       # Clustering of tweets based on text
       processedTweets = cluster_text(dist_text,processedTweets,dist_threshold_txt = (1-similarity_threshold))

       return processedTweets

if __name__ == "__main__":
     app.run(host="0.0.0.0", debug=False, port=5012)
