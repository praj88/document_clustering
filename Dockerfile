FROM ujwal19/alpine-py3-java8

# File Author / Maintainer
MAINTAINER Prajwal Shreyas

# install sdk for alpine linux
RUN apk add --update --no-cache \
    python3-dev \
    build-base

#Install jupyter kernel gateway
RUN pip install jupyter_kernel_gateway

#Install Curl
RUN apk add --update curl && \
    rm -rf /var/cache/apk/*

# Add the app file
ADD /app_document_similarity /app_document_similarity

# Get pip to download and install requirements:
RUN mv /app_document_similarity/nltk_data /usr/share
RUN pip install -r /app_document_similarity/requirements.txt


# Expose ports
EXPOSE 5012


# Set the default directory where CMD will execute
WORKDIR /app_document_similarity
CMD python3 /app_document_similarity/document_similarity.py
